<?php

namespace App\Http\Controllers;

use App\Process;
use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;

class ProcessController extends Controller
{
    /**
     * Import CVS data into database
     *
     * @param  string  $csvfile
     * @return Response
     */
    public function importcsv($csvfile){ 

       $skipfistrow = true;       
       $csvfilepath = 'csv/'.$csvfile.'.csv';

       if (($handle = fopen ( $csvfilepath, 'r' )) !== FALSE) {
        while ( ($data = fgetcsv ( $handle, 0, ',' )) !== FALSE ) {

            if($skipfistrow) { $skipfistrow = false; continue; }
           
            $process_data = new Process ();
            $process_data->id = $data [0];
            $process_data->title = $data [1];
            $process_data->price = $data [2];
            $process_data->amount = $data [3];
            $process_data->brand = $data [4];
            $process_data->ean = $data [5];
            $process_data->save ();
        
        }
        fclose ( $handle );
       }
        return  "CSV file imported !!";
        
        //return response()->json($Process); 
    }

    /**
     * Export data from database into JSON format
     *
     * @param  
     * @return Response
     */
    public function exportjson(){ 

        
        $ProcessData =  Process::all();

        return response()->json($ProcessData);

    }
}