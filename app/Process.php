<?php

namespace App;

use Illuminate\Database\Eloquent\Model;  

class Process extends Model
{   
    protected $table = 'items';

    protected $fillable = ['id', 'title', 'price', 'amount', 'brand', 'ean'];  
    public $timestamps = false;
   
}
